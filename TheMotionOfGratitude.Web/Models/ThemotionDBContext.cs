﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheMotionOfGratitude.Entites;

namespace TheMotionOfGratitude.Web.Models
{
    public class ThemotionDBContext:DbContext 
    {

        public ThemotionDBContext(DbContextOptions<ThemotionDBContext> options)
      : base(options)
        { }

        public DbSet<Product> Products { get; set; }
        public DbSet <Category> Categories { get; set; }
        public DbSet <Order> Orders { get; set; }
        
    }

    



}
