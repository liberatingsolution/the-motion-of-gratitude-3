﻿using System;
using Microsoft.AspNetCore.Mvc;




namespace TheMotionOfGratitude.Web.Controllers
{
    public class AdminController : Controller
    {
       
        public static Boolean loggedin = false;

        public IActionResult Index()
        {
           if(loggedin)
            {
                return View();
            }
           else
            {
                return RedirectToAction("Login");
            }
            
        }

        
        public IActionResult Login(string UserEmail, string UserPassword)
        {
           
            if (UserEmail=="test@email.com" && UserPassword=="123456")
            {
                //Session["UserName"];
                //Response.Cookies.Add(new HttpCookie("Day", questions.Day));
                loggedin = true;
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
            
        }

        public IActionResult Register()
        {
            return View();
        }

        public IActionResult SaveProduct()
        {
            return View();
        }

        public  IActionResult AllProducts()
        {
            return View();

        }

        public IActionResult SaveCategory()
        {
            return View();

        }

        public IActionResult AllCategory()
        {
            return View();

        }

        
    }
}