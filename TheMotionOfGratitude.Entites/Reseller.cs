﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMotionOfGratitude.Entites
{
    public class Reseller
    {

        public int ID { get; set; }
        public string StoreName { get; set; }
        public string MobileNumber { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string ProductList { get; set; }

    }
}
