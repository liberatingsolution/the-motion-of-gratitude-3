﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMotionOfGratitude.Entites
{
    public class Product
    {
        public Product()
        {
            this.DateofModified = DateTime.UtcNow;
            this.DateofCreated = DateTime.UtcNow;
        }

        public Category category;
        [Key]
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal Weight { get; set; }
        public int Stock { get; set; }
        public int? CategoryID { get; set; }
      
        public DateTime DateofModified { get; set; }
        public DateTime DateofCreated { get; set; }

        public virtual Category Category { get; set; }

    }
}
