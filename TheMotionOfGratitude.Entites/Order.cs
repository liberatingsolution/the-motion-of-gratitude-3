﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMotionOfGratitude.Entites
{
    public class Order
    {

        public int OrderID { get; set; }
        public string UserID { get; set; }
        public DateTime OrderedAt { get; set; }
        public string Status { get; set; }
        public decimal TotalAmount { get; set; }


    }
}
