﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMotionOfGratitude.Entites
{
    public class Category
    {
        public Category()
        {
            this.DateofCreated = DateTime.UtcNow;
            this.DateofModified = DateTime.UtcNow;
        }
        [Key]
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public DateTime DateofModified { get; set; }
        public DateTime DateofCreated { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }
}
